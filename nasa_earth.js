var image_cell_template = "<div class='col-sm-3 image-cell'><div class='nasa-image'><img></div><div class='image-caption'></div><div class='image-coords'></div></div>";
var date_tmpl = "begin=YEAR-MONTH-DAY";
var myApiKey = "api_key=W15nIjXMNhEtQ8FC5wJGGWeFfvHHbEoNyKgAcT3L";

var example_image_url = "https://api.nasa.gov/EPIC/archive/natural/2017/08/21/png/epic_1b_20170821151450.png?api_key=W15nIjXMNhEtQ8FC5wJGGWeFfvHHbEoNyKgAcT3L";
var epic_natural_archive_base = "https://api.nasa.gov/EPIC/archive/natural/";
var api_url_query_base = "https://epic.gsfc.nasa.gov/api/natural/date/";

// ==========================================================
// START JS: synchronize the javascript to the DOM loading
// ==========================================================
$(document).ready(function() {

  // ========================================================
  // SECTION 1:  process on click events
  // ========================================================
  $('#get-images-btn').on('click', api_search);



  // ========================================================
  // TASK 1:  build the search AJAX call on NASA EPIC
  // ========================================================
  // Do the actual search in this function
  function api_search(e) {

	// get the value of the input search text box => date
	var date = $('#search_date').val();

	// build an info object to hold the search term and API key
	var info = {};
	var date_array = date.split('-');
	info.year = date_array[0];
	info.month = date_array[1];
	info.day = date_array[2];
	info.api_key = "W15nIjXMNhEtQ8FC5wJGGWeFfvHHbEoNyKgAcT3L";

	// build the search url and sling it into the URL request HTML element
	var search_url = "https://epic.gsfc.nasa.gov/api/natural/date/" + info.year + "-" + info.month + "-" + info.day + "?api_key=" + info.api_key;
	console.log(search_url);
	// sling it!
	$('#reqURL').text(search_url)

	// make the jQuery AJAX call!
	$.ajax({
		url: search_url,
		success: function(data) {
			render_images(data,info);
		},
		cache: false
	});
}
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // ========================================================

  // ========================================================
  // TASK 2: perform all image grid rendering operations
  // ========================================================
  function render_images(data,info) {
	// get NASA earth data from search results data
	// console.log(data.length);
	var images = [];
	for (var i = 0; i < data.length; i++) {
	  // build an array of objects that captures all of the key image data

	  // => image url
	  var imgURL = data[i]['image'];

	  // => centroid coordinates to be displayed in the caption area
	  var lat = data[i]['centroid_coordinates']['lat'];
	  var lon = data[i]['centroid_coordinates']['lon'];

	  // => image date to be displayed in the caption area (under thumbnail)
	  var imgData = data[i]['date'];

	  // push to list
	  var capture = {}
	  capture.url = imgURL;
	  capture.lat = lat;
	  capture.lon = lon;
	  capture.date = imgData;
	  images.push(capture);
	}

	// select the image grid and clear out the previous render (if any)
	var earth_dom = $('#image-grid');
	earth_dom.empty();

	//render all images in an iterative loop here!
	var child = 0;
	for(var i = 0; i < images.length; i++) {
		var imgURL = "https://api.nasa.gov/EPIC/archive/natural/" + info.year + "/" + info.month + "/" + info.day + 
		"/png/" + images[i].url + ".png?api_key=" + info.api_key;

		var insert_row = `<div class='col-sm-3 image-cell'><div class='nasa-image'><img src="${imgURL}"></div>\
		<div class='image-caption'>${images[i].date}</div>\
		<div class='image-coords'>lat:${images[i].lat} lon:${images[i].lon}</div></div>`;

		if((i % 4) === 0) {
			child++;  
			$('#image-grid').append("<div class=\"row\"></div>");

		}
		$('#image-grid').children('div').eq(child-1).append(insert_row);
	}
	
}



  // ========================================================
  // TASK 3: perform single high resolution rendering
  // ========================================================
  // function to render the high resolution image
$("div").on("click", "img", function(){
	console.log('img:',this.src);
	$('#imgURL').text(this.src)
	$('#image-grid').empty();
	$('#image-grid').append("<img class='highres' src='" + this.src + "'>");

	// render your high resolution image within the #image-grid id html element
});
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // ========================================================
});
